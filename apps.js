var express = require('express')
var app = express()

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})

app.use(express.static('facility/css'))
app.use(express.static('facility/images'))
app.use(express.static('facility/js'))
app.use(express.static('facility/resources'))

app.use(express.static('./'))