function show() {
    $('#loading').show();
}

function hide() {
    $('#loading').hide();
}

$('#loading')
    .hide()  // Hide it initially
    .ajaxStart(function() {
        show();
    })
    .ajaxStop(function() {
        hide();
    })
;