const WAREHOUSE = {
    id: 22,
    pinColor: 'ba88f7',
    name: 'Warehouse' };

const ELEVATOR = {
    id: 21,
    pinColor: 'aaaaaa',
    name: 'Elevator' };

const FERTILIZER = {
    id: 41,
    pinColor: '4a96c6',
    name: 'Fertilizer' };

const MILL = {
    id: 14,
    pinColor: '833c06',
    name: 'Mill' };

const SUGAR_ETHANOL_MILL = {
    id: 61,
    pinColor: 'c7fce5',
    name: 'Sugar/Ethanol Mill' };

const OIL_PROCESSING = {
    id: 11,
    pinColor: 'fef200',
    name: 'Sugar/Ethanol Mill' };

const OIL_SEED_PROCESSING = {
    id: 10,
    pinColor: '4dee01',
    name: 'Oilseed Processing' };

const PROTEIN_PROCESSING = {
    id: 12,
    pinColor: 'fa523a',
    name: 'Protein Processing' };

const OFFICE = {
    id: 51,
    pinColor: 'eba11f',
    name: 'Office' };

const PORT = {
    id: 31,
    pinColor: 'ffffff',
    name: 'Port/Transshipment' };

const FACILITY_TYPES = [
    WAREHOUSE,
    ELEVATOR,
    FERTILIZER,
    MILL,
    SUGAR_ETHANOL_MILL,
    OIL_PROCESSING,
    OIL_SEED_PROCESSING,
    PROTEIN_PROCESSING,
    OFFICE,
    PORT];

const FACILITY_TYPES_IDS = FACILITY_TYPES.map(function(facilityType){
    return facilityType.id;
})