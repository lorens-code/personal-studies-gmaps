var FilterController = function () {


    var setDefaultSitesFilter = function () {

        $("#facilityType").empty();

        FACILITY_TYPES.forEach(function (facilityType) {

            var option = new Option(facilityType.name, facilityType.id, true, true);

            option.innerHTML = "<i class='fa fa-map-marker' style='color:#" + facilityType.pinColor + "; text-shadow: 1px 1px 2px #000000'></i> &nbsp;" + option.innerHTML;

            $("#facilityType").append($(option));

        });

        $("#facilityType").trigger("chosen:updated");

        $("input[type=radio][name='bussinessLayer']").each(function () {
            if ($(this).val() === "true") {
                $(this).prop('checked', true);
            }
        });

        $("input[type=radio][name='siteStatus']").each(function () {
            if ($(this).val() === "Active") {

                $(this).prop('checked', true);
            }
        });

        $("#siteName").val("");

        return sitesFilter = {

            siteName: "",
            status: "Active",
            isBunge: true,
            facilityTypes: FACILITY_TYPES_IDS

        }

    };

    var bindEvents = function (_this) {

        $("input[type=radio][name='siteStatus']").on('change', function () {

            _this.sitesFilter.status = $(this).val();
            mapController.hideMarkers();
            mapController.showMarkers();

        });

        $("input[type=radio][name='bussinessLayer']").on('change', function () {

            _this.sitesFilter.isBunge = $(this).val() === "true";
            mapController.hideMarkers();
            mapController.showMarkers();

        });

        $("#facilityType").on('change', function () {

            _this.sitesFilter.facilityTypes = $(this).val();
            mapController.hideMarkers();
            mapController.showMarkers();

        });

        $("#sitesFilterForm").on('submit', function (e) {
            e.preventDefault();
            _this.sitesFilter.siteName = $("#siteName").val();
            mapController.hideMarkers();
            mapController.showMarkers();
        });

        $("#resetBtn").on('click', function (e) {
            e.preventDefault();
            _this.sitesFilter = setDefaultSitesFilter();
            mapController.hideMarkers();
            mapController.showMarkers();

        });

        $('#filterContent').on('shown.bs.collapse', function () {
            mapController.resizeMap();
        });

        $('#filterContent').on('hidden.bs.collapse', function () {
            mapController.resizeMap();
        });

        $('#selectAllTypes').on('click', { booleanChoice: true }, _toogleFacilityTypes);

        $('#selectNoneTypes').on('click', { booleanChoice: false }, _toogleFacilityTypes);

    };

    var _toogleFacilityTypes = function (e) {

        e.preventDefault();

        $("#facilityType option").each(function () {

            $(this).prop('selected', e.data.booleanChoice);

        })

        $("#facilityType").trigger("chosen:updated");
        $("#facilityType").trigger("change");

    }

    return {

        sitesFilter: {},
        filteredSites: [],

        resizeChosen: function () {

            $(".chosen-container").each(function () {
                $(this).attr('style', 'width: 100%');
            })
        },

        initFilters: function () {

            this.sitesFilter = setDefaultSitesFilter();

            bindEvents(this);

        }

    }
}