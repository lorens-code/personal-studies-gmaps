function MapController() {

	var _picturesURLBase = '/facility/downloadSitePicture.do';
	
	function _getIdentifyImageURL(siteId, size) {
		
		return _picturesURLBase + '?size=' + size + '&siteId=' + siteId + '&t=' + new Date().getTime();
	}
	
    var _createMarker = function (SiteDTO, _this) {

    	var map = _this.map;
    	var infoWindows = _this.infoWindows;
    	
        var myLatLng = { lat: SiteDTO.lat, lng: SiteDTO.lon };

        var pinColor = FACILITY_TYPES.reduce(function (prev, curr) {
            return curr.id == SiteDTO.facilityTypeId ? curr.pinColor : prev;
        }, '000000');

        var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34));

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: SiteDTO.siteName,
            icon: pinImage,
            visible: false,
            optimized: false
        });

        marker.infoWindowContent = '<div id="info-' + SiteDTO.siteId + '"><div class="container infowindow-container" style="">Loading details...</div></div>';
       
        var infoWindow = new google.maps.InfoWindow({
            content: marker.infoWindowContent
        });
        
        infoWindows.push(infoWindow);

        marker.addListener('click', function () {

        	_this.closeInfoWindows();
        	
        	infoWindow.open(map, marker);

            mapService.getSiteDetail(SiteDTO.siteId, function (data) {
            	
                $("#info-" + SiteDTO.siteId).html(getInfoWindowHtml(data));
            });

        });

        marker.setMap(map);

        SiteDTO.marker = marker;

    };

    var _hideMarker = function (SiteDTO) {
        SiteDTO.marker.setVisible(false);
    };

    var _showMarker = function (SiteDTO) {
        SiteDTO.marker.setVisible(true);
    };
    
    var _zoomByViewport = function(_this) {

        if ($(window).width() >= 1600) {
            _this.map.setZoom(3);
        } else {
            _this.map.setZoom(2);
        }

        _this.map.panTo({ lat: 0, lng: 0 });

    }
    
    var _ifNullShowEmpty = function (value) {
    	return (value) ? value : "&nbsp;";
    } 

    var getInfoWindowHtml = function (SiteDetailDTO) {

    	//console.log(SiteDetailDTO);
    	
    	var capacityTable = ''
    		
    		+'<table class="table table-condensed table-responsive">'
    		+'<tr>'
    		+'<th>Facility Type</th>'
    		+'<th>Commodity</th>'
    		+'<th>Capacity Types</th>'
    		+'<th>Capacity</th>'
    		+'</tr>';
    	
    	for (var i = 0; i < SiteDetailDTO.capacityItems.length; i++) {
    		
    		capacityTable += ''
    		
    			+'<tr>'
    			
    			+ '<td>'+ _ifNullShowEmpty(SiteDetailDTO.capacityItems[i].facilityType) +'</td>'
    			+ '<td>'+ _ifNullShowEmpty(SiteDetailDTO.capacityItems[i].commodityName) +'</td>'
    			+ '<td>'+ _ifNullShowEmpty(SiteDetailDTO.capacityItems[i].capacityType) +'</td>'
    			+ '<td>'+ _ifNullShowEmpty(SiteDetailDTO.capacityItems[i].capacity) +'</td>'
    			
    			+'</tr>';
    	}
    	
    	capacityTable += '' 
    		+'</table>';
    	
        var html = ''
            + '<!-- MAIN CONT -->'
            + '<div class="container infowindow-container" style="">'

            + '<!-- IMAGE -->'
            + '<div class="row">'
            
            + '<div class="col-sm-12 col-centered">'
            + '<img style="margin: 0 auto" width="150" height="113" class="img-thumbnail center-block" src="'+ _getIdentifyImageURL(SiteDetailDTO.siteId, 1) +'" />'
            + '</div>'
            
            + '</div>'
            + '<!-- END IMAGE -->'

            + '<!-- DETAILS -->'
            + '<div class="row">'
            
            + '<!-- TABS REFERENCES -->'
            + '<ul class="nav nav-tabs">'
            
            + '<li class="active"><a data-toggle="tab" href="#address-'+SiteDetailDTO.siteId+'">Site Address</a></li>'
            + '<li><a data-toggle="tab" href="#ownership-'+SiteDetailDTO.siteId+'">Ownership</a></li>'
            + '<li><a data-toggle="tab" href="#capacity-'+SiteDetailDTO.siteId+'">Capacity</a></li>'
            
            + '</ul>'
            + '<!-- END TABS REFERENCES -->'

            + '<!-- TABS CONTENTS -->'
            + '<div class="tab-content">'

            + '<!-- ADDRESS TAB CONT -->'
            + '<div id="address-'+SiteDetailDTO.siteId+'" class="tab-pane fade in active">'
            
            + '<!-- ADDRESS CONT -->'
            + '<div class="container-fluid">'
            + '<div class="panel">'
            + '<div class="panel-body">'

            + '<!-- ADDRESS ROW -->'
            + '<div class="row">'
            + '<div class="col-xs-3">'
            + '<label>Address</label>'
            + '</div>'

            + '<div class="col-xs-9">'
            + '<p>' + _ifNullShowEmpty(SiteDetailDTO.address.address) + '</p>'
            + '</div>'

            + '</div>'
            + '<!-- END ADDRESS ROW -->'

            + '<!-- CITY ROW -->'
            + '<div class="row">'
            + '<div class="col-xs-3">'
            + '<label>City</label>'
            + '</div>'

            + '<div class="col-xs-9">'
            + '<p>' + _ifNullShowEmpty(SiteDetailDTO.address.cityName) + '</p>'
            + '</div>'

            + '</div>'
            + '<!-- END CITY ROW -->'

            + '<!-- STATE ROW -->'
            + '<div class="row">'
            + '<div class="col-xs-3">'
            + '<label>State</label>'
            + '</div>'

            + '<div class="col-xs-9">'
            + '<p>' + _ifNullShowEmpty(SiteDetailDTO.address.stateName) + '</p>'
            + '</div>'

            + '</div>'
            + '<!-- END STATE ROW -->'

            + '<!-- COUNTRY ROW -->'
            + '<div class="row">'
            + '<div class="col-xs-3">'
            + '<label>Country</label>'
            + '</div>'

            + '<div class="col-xs-9">'
            + '<p>' + _ifNullShowEmpty(SiteDetailDTO.address.countryName) + '</p>'
            + '</div>'

            + '</div>'
            + '<!-- END COUNTRY ROW -->'

            + '<!-- POSTAL CODE ROW -->'
            + '<div class="row">'
            + '<div class="col-xs-3">'
            + '<label>Postal Code</label>'
            + '</div>'

            + '<div class="col-xs-9">'
            + '<p>' + _ifNullShowEmpty(SiteDetailDTO.address.postalCode) + '</p>'
            + '</div>'

            + '</div>'
            + '<!-- END POSTAL CODE ROW -->'

        	+ '</div>'
        	+ '</div>'
            + '</div>'
            + '<!-- END ADDRESS CONT -->'
            + '</div>'
            + '<!-- END ADDRESS TAB CONT -->'

            + '<!-- OWNERSHIP TAB CONT -->'
            +'<div id="ownership-'+SiteDetailDTO.siteId+'" class="tab-pane fade in">'
            
            + '<!-- OWNERSHIP CONT -->'
            + '<div class="container-fluid">'
            + '<div class="panel">'
            + '<div class="panel-body">'

            + '<!-- TYPE ROW -->'
            + '<div class="row">'
            + '<div class="col-sm-6">'
            + '<label>Ownership Type</label>'
            + '</div>'
            
            + '<div class="col-sm-6">'
            + '<p>'+ _ifNullShowEmpty(SiteDetailDTO.ownership.type)+'</p>'
            + '</div>'
            
            + '</div>'
            + '<!-- END TYPE ROW -->'
            
            + '<!-- ASSOCIATED ROW -->'
            + '<div class="row">'
            + '<div class="col-sm-6">'
            + '<label>Associated Partner Name</label>'
            + '</div>'
            
            + '<div class="col-sm-6">'
            + '<p>'+ _ifNullShowEmpty(SiteDetailDTO.ownership.partnerName)+'</p>'
            + '</div>'
            
            + '</div>'
            + '<!-- END ASSOCIATED ROW -->'
            
            + '<!-- COMP ROW -->'
            + '<div class="row">'
            + '<div class="col-sm-6">'
            + '<label>Operating Company</label>'
            + '</div>'
            
            + '<div class="col-sm-6">'
            + '<p>'+ _ifNullShowEmpty(SiteDetailDTO.ownership.companyName) +'</p>'
            + '</div>'
            
            
            + '</div>'
            + '<!-- END COMP ROW -->'

            + '</div>'
            + '</div>'
            + '</div>'
            + '<!-- END OWNERSHIP CONT -->'
            
            + '</div>'
            + '<!-- END OWNERSHIP TAB CONT -->'


            + '<!-- CAPACITY TAB CONT -->'
            + '<div id="capacity-'+SiteDetailDTO.siteId+'" class="tab-pane fade in">'
            
            + capacityTable
            
            + '</div>'
            + '<!-- END CAPACITY TAB CONT -->'


            + '</div>'
            + '<!-- END TABS CONTENTS -->'

            + '</div>'
            + '<!-- END DETAILS -->'

            + '</div>'
            + '<!-- END MAIN CONT -->'

        return html;

    }

    return {

        map: {},
        sites: [],
        infoWindows: [],

        showMarkers: function () {

            filterService
                .filterSites(sites, filterController.sitesFilter)
                .forEach(_showMarker);
        },

        createMarkers: function () {

            var _this = this;

            sites.forEach(function (item) {
                _createMarker(item, _this);
            });

        },

        hideMarkers: function () {
            sites.forEach(function (item) {
                _hideMarker(item);
            });
        },

        setSites: function (fetchedSites) {
            sites = fetchedSites;
        },
        
        closeInfoWindows : function () {
        	
        	this.infoWindows.forEach(function(iw){
        		iw.close();
        	});
        },

        resizeMap: function () {

            filterController.resizeChosen();

            var windowHeight = $(window).height();
            var contentHeight = $("#contentWrapper").height();
            var mapCanvasHeight = $("#map").height();
            var topBarHeight = $("#topBar").height();

            var emptyContentHeight = (contentHeight - mapCanvasHeight);

            var availableHeight = windowHeight - emptyContentHeight - topBarHeight;

            $("#map").height(availableHeight);

            _zoomByViewport(this);

            google.maps.event.trigger(map, "resize");
            
        }

    }

}