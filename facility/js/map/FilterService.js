// TODO return as function(){return Object}

// because I.E. ¬¬
var _includes = function (subject, search) {
    
    start = 0;

    if (start + search.length > subject.length) {
        return false;
    } else {
        return subject.indexOf(search, start) !== -1;
    }

};

var filterService = {

    filterSites: function (sites, sitesFilter) {

        var returnVal = sites
            .filter(this._filterSiteName(sitesFilter.siteName))
            .filter(this._filterStatus(sitesFilter.status))
            .filter(this._filterIsBunge(sitesFilter.isBunge))
            .filter(this._filterfacilityTypes(sitesFilter.facilityTypes));

        //console.log(returnVal);

        return returnVal;

    },

    _filterSiteName: function (siteName) {
        return function (SiteDTO) {

            var strSiteName = SiteDTO.siteName + "";

            if ( siteName && _includes(strSiteName.toLowerCase(), siteName.toLowerCase()) ) {

                return SiteDTO;

            } else if (!siteName) {

                return SiteDTO;

            }
        }
    },

    _filterStatus: function (status) {
        return function (SiteDTO) {
            if (SiteDTO.status == status) return SiteDTO;
        }
    },

    _filterIsBunge: function (isBunge) {
        return function (SiteDTO) {
            if (SiteDTO.isBunge == isBunge) return SiteDTO;
        }
    },

    _filterfacilityTypes: function (facilityTypes) {

        return function (SiteDTO) {

            var arrResult = facilityTypes.filter(function (value) {

                if (SiteDTO.allFacilityTypeIds.indexOf(value) >= 0)
                    return value;

            });

            if (arrResult.length > 0) {
                return SiteDTO;
            }

        }
    }



}