var mapController = MapController();
var filterController = FilterController();

var initMap = function () {

    mapController.map = new google.maps.Map(document.getElementById('map'), {

        center: {
            lat: 0,
            lng: 0
        },

        zoom: 3,
        minZoom: 2,
        scrollwheel: false

    });

    mapController.map.addListener('zoom_changed', function(){
        mapController.map.panTo({ lat: 0, lng: 0 })
    });

    mapController.map.setCenter();

    mapController.resizeMap();

    //mapService.getSiteLocations(initSites);

initSites();
};

var initSites = function (data) {

    filterController.initFilters();

    //mapController.setSites(data);
    //mapController.createMarkers();
    //mapController.showMarkers();

}

// TODO : set to a WindowController
var id;

$(window).resize(function() {
    clearTimeout(id);
    id = setTimeout(doneResizing, 500);
});

function doneResizing(){
  mapController.resizeMap();   
}