var mapService = {

    getSiteLocations: function (callback) {
        return $.ajax({
            url: '/facility/resources/siteLocations.do',
            crossdomain: true
        })
        .done(function (data) { callback(data) });
    },

    getSiteDetails: function (siteId, callback) {
        return $.ajax({
            url: '/facility/resources/siteDetails.do',
            data: {
                "siteId" : siteId
            },
            crossdomain: true
        })
        .done(function (data) { callback(data) });
    },

}